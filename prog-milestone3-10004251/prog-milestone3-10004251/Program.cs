﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_1004251
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine("**********************************************");
            Console.WriteLine("Hello are you ready to order?");
            Console.WriteLine("1. Yes");
            Console.WriteLine("2. No");
            Console.WriteLine("**********************************************");
            int y = Convert.ToInt32(Console.ReadLine());

            if (y == 1)
            {
                goto Start;
            }

            else if (y == 2)
            {
                Console.WriteLine("Please come back when you are ready");
                Environment.Exit(0);
            }

        Start:
            var name = new List<string>();
            var number = new List<string>();

            Console.WriteLine("Hello, what is your name?");
            name.Add(Console.ReadLine());
            Console.WriteLine("Thankyou, what is number?");
            number.Add(Console.ReadLine());

            foreach (var x in name)
            {
                Console.WriteLine($"Hello, is {x} your name? Reply with yes or no");
            }
            var i = Console.ReadLine();

            if (i == "no")
            {
                name.Remove(",");
                Console.WriteLine("Re enter your name");
                name.Add(Console.ReadLine());
            }

            foreach (var x in number)
            {
                Console.WriteLine($"Hello, is {x} your number? Reply with yes or no");
            }

            var z = Console.ReadLine();

            if (z == "yes")
            {
                Order();
            }

            else if (z == "no")
            {
                number.Remove(",");
                Console.WriteLine("Re enter your number");
                number.Add(Console.ReadLine());
            }
            var type = new List<string> { "Hawaiian  ", "MeatLover ", "Supreme   ", "Vegetarian" };

            var order2 = new List<Tuple<string, Double>>();
            var c = "y";
            while (c == "y") ;


        }

        static void Order()
        {
            var pricetotal = 0.00;

            var PizzaSelection = new List<Tuple<int, string, Double, string>>();

            PizzaSelection.Add(Tuple.Create(0, "Vege", 4.99, "small"));
            PizzaSelection.Add(Tuple.Create(1, "Meaty Goodness", 4.99, "small"));
            PizzaSelection.Add(Tuple.Create(2, "Cheese", 4.99, "small"));
            PizzaSelection.Add(Tuple.Create(3, "Mystery", 4.99, "small"));
            PizzaSelection.Add(Tuple.Create(4, "Vege", 7.99, "medium "));
            PizzaSelection.Add(Tuple.Create(5, "Meaty Goodness", 7.99, "medium "));
            PizzaSelection.Add(Tuple.Create(6, "Cheese", 7.99, "medium "));
            PizzaSelection.Add(Tuple.Create(7, "Mystery", 7.99, "medium"));
            PizzaSelection.Add(Tuple.Create(8, "Vege", 10.99, "Large "));
            PizzaSelection.Add(Tuple.Create(9, "Meaty Goodness", 10.99, "Large "));
            PizzaSelection.Add(Tuple.Create(10, "Cheese", 10.99, "Large "));
            PizzaSelection.Add(Tuple.Create(11, "Mystery", 10.99, "Large "));


            Console.WriteLine("We will now take your order, please choose a pizza");
            foreach (var x in PizzaSelection)
            {

                Console.WriteLine($"{x.Item1 + 1} A {x.Item4} {x.Item2} Pizza is ${x.Item3}");
            }

            var pizzaorder = new List<Tuple<string, Double, string>>();
            var a = "yes";
            while (a == "yes")
            {

                Console.WriteLine("Select the number of the pizza you want");
                var b = int.Parse(Console.ReadLine());

                if (b == 1)
                {
                    pizzaorder.Add(Tuple.Create("Vege", 4.99, "Small"));
                }
                else if (b == 2)
                {
                    pizzaorder.Add(Tuple.Create("Meaty Goodness", 4.99, "Small"));
                }
                else if (b == 3)
                {
                    pizzaorder.Add(Tuple.Create("Cheese", 4.99, "small"));
                }
                else if (b == 4)
                {
                    pizzaorder.Add(Tuple.Create("Xtreme Cheese", 4.99, "small"));
                }
                else if (b == 5)
                {
                    pizzaorder.Add(Tuple.Create("Vege", 7.99, "medium "));
                }
                else if (b == 6)
                {
                    pizzaorder.Add(Tuple.Create("Meaty Goodness", 7.99, "medium "));
                }
                else if (b == 7)
                {
                    pizzaorder.Add(Tuple.Create("Cheese", 7.99, "medium "));
                }
                else if (b == 8)
                {
                    pizzaorder.Add(Tuple.Create("Xtreme Cheese", 7.99, "medium"));
                }
                else if (b == 9)
                {
                    pizzaorder.Add(Tuple.Create("Vege", 10.99, "Large "));
                }
                else if (b == 10)
                {
                    pizzaorder.Add(Tuple.Create("Meaty Goodness", 10.99, "Large "));
                }
                else if (b == 11)
                {
                    pizzaorder.Add(Tuple.Create("Cheese", 10.99, "Large "));
                }
                else if (b == 11)
                {
                    pizzaorder.Add(Tuple.Create("Mystery", 10.99, "Large "));
                }

                Console.WriteLine("Do you wish to order another pizza? yes/no");
                a = Console.ReadLine();
            }
            {
                var order2 = new List<Tuple<string, Double>>();
                var c = "yes";
                while (c == "yes")
                {
                    var drinks = new List<Tuple<int, string, Double>>();
                    drinks.Add(Tuple.Create(1, "Water", 0.00));
                    drinks.Add(Tuple.Create(2, "Pink lemonade", 3.99));
                    drinks.Add(Tuple.Create(3, "GFuel", 8.00));

                    Console.WriteLine("Here is our thirst quenching drinks");

                    foreach (var x in drinks)
                    {

                        Console.WriteLine($" mumber {x.Item1} A {x.Item2} is ${x.Item3}");
                    }

                    Console.WriteLine("Choose a drink");
                    var drinks2 = int.Parse(Console.ReadLine());


                    if (drinks2 == 1)
                    {
                        order2.Add(Tuple.Create("Water", 0.00));
                    }
                    else if (drinks2 == 2)
                    {
                        order2.Add(Tuple.Create("Pink lemonade", 3.99));
                    }
                    else if (drinks2 == 3)
                    {
                        order2.Add(Tuple.Create("GFuel", 8.00));
                    }
                    Console.WriteLine("Is that all? yes/no");
                    c = Console.ReadLine();
                    Environment.Exit(0);
                }
            }
        }
    }
}